<?php if(!defined('PLX_ROOT')) exit;
class baby extends plxPlugin{
    public function __construct($default_lang) {#var_dump(__CLASS__.'->'.__FUNCTION__);$e = new \Exception;var_dump($e->getTraceAsString());#exit(__LINE__.' '.__FILE__);
        parent::__construct($default_lang);# Appel du constructeur de la classe plxPlugin (obligatoire)
        $this->spxname="baby";
        $this->root = $this->baby = FALSE;
        if(!defined('PLX_ADMIN')) {# Déclarations des hooks
            $this->addHook('Index', 'Index');
            //$this->addHook('plxMotorConstruct', 'Index');
            $this->addHook('plxShowPluginsCss', 'plxShowPluginsCss');
            $this->addHook('baby::getcss', 'getcss');
            $this->addHook('baby::getjs', 'getjs');
            $this->addHook('baby::header', 'header');
            $this->addHook('baby::template', 'template');
            $this->addHook('baby::footer', 'footer');
        }else{
            $this->addHook('plxAdminEditConfiguration', 'plxAdminEditConfiguration');
            $this->addHook('AdminPrepend', 'AdminPrepend');
        }
    }


    /**
     * Maintient le theme Enfant ds la config globale : $global['style'] lors des modifs : parametres_(base|avanaces|...)
     * Scope    plxAdmin editConfiguration($global,$content) : Méthode qui édite le fichier XML de configuration selon le tableau $global et $content
     * Hook     plxAdminEditConfiguration
     * @param	global	tableau contenant toute la configuration PluXml
     * @param	content	tableau contenant la configuration à modifier
     * @return	Void
     * @author	Thomas Ingles
     **/
    public function plxAdminEditConfiguration() {
        echo '<?php $babePlugName = "'.__CLASS__.'";'; ?>
            $babePlug = $this->plxPlugins->aPlugins[$babePlugName];
#var_dump( __FILE__,'plxAdminEditConfiguration', $babePlugName, @$babePlug->baby, $this->style, $global, $content);EXIT;
            if($babePlug->baby AND $babePlug->baby != $this->style) {#$babePlug->baby = False OR 'baby style name'
                $global['style'] = $babePlug->baby;# Return to baby theme
            }
?><?php
    }
    public function AdminPrepend(){#parametres_themes
        echo '<?php '; ?>
            $chk_thm_prm = (FALSE === strpos($plxAdmin->path_url,'parametres_themes') AND FALSE === strpos($plxAdmin->path_url,'parametres_edittpl'));
            $configtheme = PLX_ROOT.$plxAdmin->aConf["racine_themes"].$plxAdmin->aConf["style"]."/child.php";
#var_dump(__FILE__,'AdminPrepend BABY',$configtheme);
            if($chk_thm_prm AND file_exists($configtheme) AND filesize($configtheme) > 0) {
#var_dump(__FILE__,'AdminPrepend BABY exists',$configtheme);
                require($configtheme);
                $plxAdmin->plxPlugins->aPlugins['<?php echo __CLASS__ ?>']->root = $plxAdmin->aConf['racine_themes'];
                $plxAdmin->plxPlugins->aPlugins['<?php echo __CLASS__ ?>']->baby = $plxAdmin->aConf['style'];
                $plxAdmin->aConf["style"] = PLX_PARENT_THEME;
                $plxAdmin->style = PLX_PARENT_THEME;
                if(!defined("PLX_PARENT_THEME_PLUGIN_CSS")){
                    define("PLX_PARENT_THEME_PLUGIN_CSS", false);
                }
            }
?><?php
    }
    public function Index(){#var_dump(__CLASS__.'->'.__FUNCTION__);$e = new \Exception;var_dump($e->getTraceAsString());#exit(__LINE__.' '.__FILE__);
        echo '<?php '; ?>
            $configtheme = PLX_ROOT.$plxMotor->aConf["racine_themes"].$plxMotor->aConf["style"]."/child.php";
#var_dump(__FILE__,'INDEX BABY',$configtheme);
            if(file_exists($configtheme) AND filesize($configtheme) > 0) {
#var_dump(__FILE__,'INDEX BABY exists',$configtheme);
                require($configtheme);
                $plxMotor->plxPlugins->aPlugins['<?php echo __CLASS__ ?>']->root = $plxMotor->aConf['racine_themes'];
                $plxMotor->plxPlugins->aPlugins['<?php echo __CLASS__ ?>']->baby = $plxMotor->aConf['style'];
                $plxMotor->aConf["style"] = PLX_PARENT_THEME;
                $plxMotor->style = PLX_PARENT_THEME;
                if(!defined("PLX_PARENT_THEME_PLUGIN_CSS")){
                    define("PLX_PARENT_THEME_PLUGIN_CSS", false);
                }
            } //exit(__LINE__.' '.__FILE__);
?><?php
    }

    public function template($parms = FALSE){#var_dump(__CLASS__.'->'.__FUNCTION__);$e = new \Exception;var_dump($e->getTraceAsString());#exit(__LINE__.' '.__FILE__);
        $file = (is_array($parms))?$parms[0]:0;
        echo '<?php '; ?>
        $baby_file = ('<?php echo $file?>'?'<?php echo $file?>.php':$plxMotor->template);
#var_dump(__FILE__,'template BABY',$baby_file);
        if(file_exists('<?php echo PLX_ROOT.$this->root.$this->baby ?>/'.$baby_file)) {
            $plxMotor->aConf['style'] = '<?php echo $this->baby ?>';
            $plxMotor->style = '<?php echo $this->baby ?>';
            include('<?php echo PLX_ROOT.$this->root.$this->baby ?>/'.$baby_file);
            $plxMotor->aConf['style'] = PLX_PARENT_THEME;
            $plxMotor->style = PLX_PARENT_THEME;
            return true;
        }
?><?php
    }
    public function header(){#var_dump(__CLASS__.'->'.__FUNCTION__);$e = new \Exception;var_dump($e->getTraceAsString());#exit(__LINE__.' '.__FILE__);
        if(file_exists(PLX_ROOT.$this->root.$this->baby.'/header.php')) {
            echo '<?php '; ?>
#var_dump(__FILE__,'header BABY','<?php echo $this->baby ?>');
            $plxMotor->aConf['style'] = '<?php echo $this->baby ?>';
            $plxMotor->style = '<?php echo $this->baby ?>';
            include('<?php echo PLX_ROOT.$this->root.$this->baby.'/header.php' ?>');
            $plxMotor->aConf['style'] = PLX_PARENT_THEME;
            $plxMotor->style = PLX_PARENT_THEME;
            return true;
?><?php
        }
    }
    public function footer(){#var_dump(__CLASS__.'->'.__FUNCTION__);$e = new \Exception;var_dump($e->getTraceAsString());#exit(__LINE__.' '.__FILE__);
        if(file_exists(PLX_ROOT.$this->root.$this->baby.'/footer.php')) {
            echo '<?php '; ?>
#var_dump(__FILE__,'footer BABY','<?php echo $this->baby ?>');
            $plxMotor->aConf['style'] = '<?php echo $this->baby ?>';
            $plxMotor->style = '<?php echo $this->baby ?>';
            include('<?php echo PLX_ROOT.$this->root.$this->baby.'/footer.php' ?>');
            $plxMotor->aConf['style'] = PLX_PARENT_THEME;
            $plxMotor->style = PLX_PARENT_THEME;
            return true;
?><?php
        }
    }
    public function getcss(){#var_dump(__CLASS__.'->'.__FUNCTION__);$e = new \Exception;var_dump($e->getTraceAsString());#exit(__LINE__.' '.__FILE__);//Méthode principale qui retourne le css
        $plxMotor = plxMotor::getInstance();
        $hrefcss = $plxMotor->urlRewrite($plxMotor->aConf['racine_themes'].$this->baby)."/child.css";
        $s = '<link rel="stylesheet" href="'.$hrefcss.'" media="screen"/>'.PHP_EOL;
        return $s;
    }
    public function getjs(){#var_dump(__CLASS__.'->'.__FUNCTION__);$e = new \Exception;var_dump($e->getTraceAsString());#exit(__LINE__.' '.__FILE__);#Méthode principale qui retourne le js
        $plxMotor = plxMotor::getInstance();
        $hrefjs = $plxMotor->urlRewrite($plxMotor->aConf['racine_themes'].$this->baby)."/child.js";
        $s = '<script type="text/javascript" src="'.$hrefjs.'" ></script>'.PHP_EOL;
        return $s;
    }
    public function plxShowPluginsCss(){#var_dump(__CLASS__.'->'.__FUNCTION__);$e = new \Exception;var_dump($e->getTraceAsString());#exit(__LINE__.' '.__FILE__);
        if (defined("PLX_PARENT_THEME_PLUGIN_CSS") && PLX_PARENT_THEME_PLUGIN_CSS == true) echo ($this->getcss());
    }
}
