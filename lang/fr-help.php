<?php if(!defined('PLX_ROOT')) exit;//todo include this head for next trad
$pluginName = end($_GET);//multiple pluxml
include(PLX_PLUGINS.$pluginName.'/lang/help.inc.php');#inclure les communs
?>
<div id="baby_help">
 <h3>Description :</h3>
 <p>Ce plugin va vous permettre de créer des thèmes enfants de vos thèmes préférés. Un peu à la manière de wordpress mais en beaucoup plus simple.</p>
 <p>Plus d'infos sur thèmes parents enfants ici : <a href="https://wpformation.com/theme-enfant-wordpress/" >theme-enfant-wordpress</a></p>
 <p>L'idée est d'avoir un thème parent et un thème enfant dans le dossier themes de pluxml : le thème enfant surcharge le css et/ou js du thème parent. C'est le thème enfant qui est le thème sélectionné.
La mise a jour du parent n'a aucune influence puisque la surchage est faite dans le thème enfant.</p>
 <p>Pour que ça fonctionne il faut installer ce plugin. Aucune configuration n'est nécessaire.</p>
 <h3>Mise en place du theme enfant :</h3>
 <p>Nous créons un dossier enfant dans themes themes/childdefault/</p>
 <p>Exemple : Dans le dossier childdefault et à la racine vous créer les fichiers suivants</p>
 <ul class="">
  <li>Un fichier child.css</li>
  <li>Un fichier child.js</li>
  <li>Un fichier child.php</li>
 </ul>
 <p>Pour les deux premiers c'est facile vous mettez respectivement votre css et javascript pour forcer le thème parent.</p>
 <p>Pour child.php vous éditez et vous mettez le nom du dossier de votre parent :</p>
 <pre>&lt;?php define('PLX_PARENT_THEME', 'defaut'); ?></pre>
 <h3>Si besoin est :</h3>
 <p>vous pouvez definir une constante qui retiendra l'adresse de base du theme parent comme ceci</p>
 <pre>&lt;?php define('PLX_PARENT_THEME_URL', $plxMotor->urlRewrite($plxMotor->aConf['racine_themes'].PLX_PARENT_THEME)); ?&gt;</pre>
<h3>Mise en place du theme parent :</h3>
 <p>Nous allons maintenant rajouter deux hook dans le header du thème parent pour qu'il prenne en compte le css et js de l'enfant</p>
 <p>Dans le thème default (parent) ouvrez le header.php:</p>
 <p>On commence par le css, à la place de : </p>
 <pre>&lt;link rel=&quot;stylesheet&quot; href=&quot;&lt;?php $plxShow-&gt;template(); ?&gt;/css/plucss.css&quot; media=&quot;screen&quot;/&gt;<br />
&lt;link rel=&quot;stylesheet&quot; href=&quot;&lt;?php $plxShow-&gt;template(); ?&gt;/css/theme.css&quot; media=&quot;screen&quot;/&gt;</pre>
 <p>Vous ajouter le hook en dessous ce qui donne : </p>
 <pre>&lt;link rel=&quot;stylesheet&quot; href=&quot;&lt;?php $plxShow-&gt;template(); ?&gt;/css/plucss.css&quot; media=&quot;screen&quot;/&gt;<br />
&lt;link rel=&quot;stylesheet&quot; href=&quot;&lt;?php $plxShow-&gt;template(); ?&gt;/css/theme.css&quot; media=&quot;screen&quot;/&gt;<br />
&lt;?php echo ($plxShow-&gt;callHook('baby::getcss')); ?&gt;
 </pre>
 <p>Puis le javascript : </p>
 <pre>&lt;?php echo ($plxShow-&gt;callHook('baby::getjs')); ?&gt;</pre>
 <p>Il existe une deuxième solution plus simple pour le css</p>
 <p>dans child.php vous éditez et vous rajouter la constante suivante :</p>
 <pre>&lt;?php define('PLX_PARENT_THEME_PLUGIN_CSS', 'true'); ?></pre>
 <p>Le css sera chargé directement dans le thème parent via:  &lt;?php $plxShow->pluginsCss() ?>. Dans ce cas vous n'avez pas à mettre le hook spécifique (baby::getcss) dans le thème parent.</p>
 <p>Ne pas oublier <strong>d'activer le thème enfant</strong> et non le thème parent !</p>
 <p>Simple non ?</p>
<h2>Avancé Surcharge de certain fichiers du thème parent</h2>
<pre>
1.0.1 SWD (overide specific parent theme files)
+ in parent header ::: basic
if (eval($plxShow-&gt;callHook('baby::header'))) return;

+ in parent footer ::: basic
if (eval($plxShow-&gt;callHook('baby::footer'))) return;

+ &lt;?php (header/footer or other template included
if (eval($plxShow-&gt;callHook('baby::template',array('header-specific')))) return;
in parent file theme (header-specific.php (for exemple)) : After : <i>if(!defined('PLX_ROOT')) exit;</i> IS A Goog think

[code]
<code>&lt;?php if(!defined('PLX_ROOT')) exit; if (eval($plxShow-&gt;callHook('baby::template',array('header-specific')))) return; ?&gt;</code>

+ &lt;?php (home/article/static or other template
if (eval($plxShow-&gt;callHook('baby::template'))) return;
before include(dirname(__FILE__) . '/header.php');

+ &lt;?php (sidebar or other template included
if (eval($plxShow-&gt;callHook('baby::template',array('sidebar')))) return;
in parent file theme (sidebar.php (for exemple)) : After :<i>if(!defined('PLX_ROOT')) exit;</i> IS A Goog think

[code]
<code>&lt;?php if(!defined('PLX_ROOT')) exit; if (eval($plxShow-&gt;callHook('baby::template',array('sidebar')))) return; ?&gt;</code>

</pre>

